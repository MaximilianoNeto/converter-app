var converter = {
  init: function () {
    this.bind();
  },

  bind: function () {
    var _this = this;
    $('#submit').on('click', function (event) {
      event.preventDefault();
      _this.check();
    });
  },

  check: function () {
    var number = $('#number_value');
    var base = $('#base_type');
    var expected = $("#base_expected");
    var result = $('#result');

    if (this.validator(number, base, expected)) {
      this.convertTo(number, base, expected);
    } else {
      result.val("");
    }
  },

  validator: function ($numberInput, $baseInput, $expectedBaseInput) {
    var numberValue = $numberInput.val();
    var numberBase = $baseInput.val();
    var expectedBase = $expectedBaseInput.val();
    var binaryRegex = /^[0-1]{1,}$/;
    var hexRegex = /^[0-9a-f]+$/;
    var numberRegex = /^\d*\.?\d*$/;
    var octalRegex = /^[1-7][0-7]*$/;

    var isBinary = binaryRegex.test(numberValue);
    var isHex = hexRegex.test(numberValue.toLowerCase());
    var isNumber = numberRegex.test(numberValue);
    var isOctal = octalRegex.test(numberValue);

    if (!numberValue || (!isBinary && !isHex)) {
      $numberInput.removeClass("is-valid").addClass("is-invalid");
      return false;
    }

    if (numberBase == expectedBase) {
      $baseInput.removeClass("is-valid").addClass("is-invalid");
      $expectedBaseInput.removeClass("is-valid").addClass("is-invalid");
      return false;
    }

    if (numberBase == 2 && (!isBinary)) {
      $numberInput.removeClass("is-valid").addClass("is-invalid");
      return false;
    }

    if (numberBase == 10 && (!isNumber)) {
      $numberInput.removeClass("is-valid").addClass("is-invalid");
      return false;
    }
    
    if (numberBase == 8 && !isOctal) {
      $numberInput.removeClass("is-valid").addClass("is-invalid");
      return false;
    }

    $baseInput.addClass("is-valid").removeClass("is-invalid");
    $expectedBaseInput.addClass("is-valid").removeClass("is-invalid");
    $numberInput.addClass("is-valid").removeClass("is-invalid");

    return true;
  },
  convertTo: function ($numberInput, $baseInput, $expectedBaseInput) {
    var result = $('#result');

    var number = $numberInput.val();
    var base = $baseInput.val();
    var expectedBase = $expectedBaseInput.val();

    var convert = parseInt(number, base).toString(expectedBase);
    if (isNaN(convert) && !(expectedBase == 16)) {
      $numberInput.removeClass("is-valid").addClass("is-invalid");
      result.val("");
      return;
    }
    result.val(convert.toUpperCase());
  }
};

$(document).ready(function () {
  converter.init();
});