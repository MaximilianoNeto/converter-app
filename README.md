# ConverterApp

[Demo](https://conversor.maximilianoveiga.com.br/) - Project demo.

This is a binary, octal, decimal, hexadecimal converter make in javascript with gulp.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

A step by step series of commands that tell you how to get the project running.

```
npm install
```

And to run the project, all you need is type:

```
npm start
```

## Built With

* [Node.js](https://nodejs.org/en/) - The web framework used.
* [Gulp](https://gulpjs.com/) - The main compiler.

## Author

* **Maximiliano Neto** - *Author* - [Porfolio](https://maximilianoveiga.com.br)

## License

This project is licensed under the GNU License - see the [LICENSE.md](LICENSE.md) file for details

